INTRODUCTION
------------

The Modular Webforms module lets content administrators group webform components into "module" forms which can in turn be added
to other webforms by adding a module component. In essence this permits content administrators to build reusable
libraries of webform component definitions.

This is useful in situations where many content administrators need to created
webforms with standardized groups of fields (surveys for example) but may need to adjust the order of appearance of groups of
fields from one form to the next.


REQUIREMENTS
------------

This module requires the Webform module.


INSTALLATION
------------

Standard. See https://www.drupal.org/documentation/install/modules-themes/modules-7

CONFIGURATION
-------------



TROUBLESHOOTING
---------------



MAINTAINERS
-----------

Allen Freeman
Drupal user: afreemannc
afreemannc@gmail.com
