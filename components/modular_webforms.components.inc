<?php

/**
 * @file
 * Webform hooks for defining the Webform Module component.
 */

/**
 * Implements _webform_defaults_COMPONENT_TYPE().
 */
function _webform_defaults_modular_webform() {
  return array(
    'name' => 'Webform Module',
    'form_key' => '',
    'pid' => 0,
    'weight' => 0,
    'extra' => array(
      'collapsible' => 0,
      'collapsed' => 0,
      'description' => '',
      'title_display' => 0,
      'private' => 0,
    ),
  );
}

/**
 * Implements _webform_edit_COMPONENT_TYPE().
 */
function _webform_edit_modular_webform($component) {
  $modules = modular_webforms_get_available_modules();
  $form['module_select'] = array(
    '#type' => 'select',
    '#title' => t('Select a module'),
    '#options' => $modules,
    '#required' => TRUE,
  );
  $form['append_pagebreak'] = array(
    '#type' => 'checkbox',
    '#title' => t('Append pagebreak'),
    '#description' => t('If checked, a page break component will be added after the module fields.'),
  );
  $form['#validate'][] = '_modular_webform_validate_component';
  return $form;
}

/**
 * Validation callback stashes the selected module node id in extra.
 */
function _modular_webform_validate_component($form, &$form_state) {
  // Doing it this way means Webform stores our component data automatically.
  $form_state['values']['extra']['module_select'] = isset($form_state['values']['module_select']) ? $form_state['values']['module_select'] : 0;
}
