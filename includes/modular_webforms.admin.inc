<?php
/**
 * @file
 * Admin settings form and related functions.
 */


/**
 * Admin settings form.
 */
function modular_webforms_admin_settings_form() {
  $webform_types = webform_node_types();
  $options = array();
  foreach ($webform_types as $type) {
    $options[$type] = $type;
  }
  $enabled_types = variable_get('modular_webforms_node_types', array());
  $form['intro'] = array(
    '#markup' => '<p>' . t('Configure Modular Webforms.') . '</p>',
  );
  $form['modular_webforms_node_types'] = array(
    '#title' => t('Select webform content types to use as modules.'),
    '#description' => t('Any content type selected here will be listed as available modules when creating audit forms.'),
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => variable_get('modular_webforms_node_types', array()),
  );
  return system_settings_form($form);
}
